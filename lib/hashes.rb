# EASY
require 'byebug'
# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_lengths = Hash.new
  str.split(' ').each do |word|
    word_lengths[word] = word.length
  end
  word_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k,v| v}[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each {|k,v| older[k] = v}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_count = Hash.new
  letters = word.chars.uniq
  letters.each do |ch|
    letter_count[ch] = word.count(ch)
  end
  letter_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_el = Hash.new
   arr.each do |el|
     uniq_el[el] = true
   end
   uniq_el.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  count = {:even => 0, :odd => 0}

  numbers.each do |number|
    count[:even] += 1 if number.even?
    count[:odd] += 1 if number.odd?
  end

  count
end


# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = 'aeiou'
  vowel_count = Hash.new(0)
  string_vowels = string.split('').select {|ch| vowels.include?(ch)}
  string_vowels.each do |vowel|
    vowel_count[vowel] += 1
  end
  vowel_count.select do |k, v|
    v == (vowel_count.sort_by{|k,v| v}.last.last)
  end.sort_by {|k,v| k}.first.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  result = []
 sec_half = students.select {|k,v| v > 6}.keys
 sec_half.each_with_index do |student, idx|
   sec_half.each_with_index do |student2, idx2|
     unless student == student2 || result.include?([student, student2].sort)
       result << [student, student2].sort
     end
   end
 end
 result
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula:
# number_of_species**2 * smallest_population_size / largest_population_size
# biodiversity_index(["cat", "cat", "cat"]) => 1
# biodiversity_index(["cat", "leopard-spotted ferret", "dog"]) => 9
def biodiversity_index(specimens)
  bio_index = Hash.new(0)
  specimens.each do |animal|
    bio_index[animal] += 1
  end
  number_of_species = bio_index.keys.count
  smallest_pop = bio_index.values.sort.first
  largest_pop = bio_index.values.sort.last
  (number_of_species**2) * smallest_pop / largest_pop
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)

  letter_count = Hash.new(0)
  normal_sign = remove_punc_cap(normal_sign)

  normal_sign.chars.each do |ch|
    letter_count[ch] += 1
  end

  vandalized_sign.downcase.chars.each do |ch|
    if letter_count.keys.include?(ch)
      if letter_count[ch] < 1
        return false
      end
     letter_count[ch] -= 1
    else
      return false
    end
  end

 true
end


def remove_punc_cap(str)
  punctuation = ";:,.?!'"
  str.downcase.delete!(punctuation)
end
